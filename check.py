#!/usr/bin/python3

import sys
from yamllint.config import YamlLintConfig
import yamllint.linter

class StgnParseError(Exception):
    "STGN parse error"
class StgnMetadataParseError(StgnParseError):
    "STGN parse error of YAML metadata"
    def __init__(self, yamlerrors):
        self.yamlerrors = yamlerrors

def split_load(full_contents):
    """Return STGN metadata and game data identified in `full_contents`.

    STGN metadata is YAML markup starting with "---\n", and extend up
    to a second "---\n" marker, which is not part of metadata or
    header.  The game data extends from after this marker to the end
    of the file content.

    Anything before the initial marker is ignored.
    """
    try:
        marker0 = full_contents.index("---\n")
    except ValueError:
        raise StgnParseError("metadata header not found")

    try:
        marker1 = full_contents.index("---\n", 4)
    except ValueError:
        raise StgnParseError("end of metadata not found")

    print("markers: %s %s" % (marker0, marker1))
    return full_contents[marker0:marker1], full_contents[marker1+3:]

def check_stgn(fname):
    with open(fname) as f:
        full_contents = f.read()
    metadata, gamedata = split_load(full_contents)

    yamllint_conf = YamlLintConfig('extends: default')
    yamlerrors = []
    for yamlerror in yamllint.linter.run(metadata, yamllint_conf):
        # FIXME this shows line numbers assuming no data before initial "---"
        yamlerrors.append(str(yamlerror))
    if yamlerrors:
        raise StgnMetadataParseError(yamlerrors)
    
if __name__ == "__main__":
    try:
        check_stgn(sys.argv[1])
    except StgnMetadataParseError as ex:
        print("Problems detected in metadata:\n  %s" % '  '.join(ex.yamlerrors))
    except StgnParseError as ex:
        print("Problem detected: %s" % ex.args[0])
