# hex coordinate system

Individual hexes are named using a coordinates system illustrated
below, similar to several hexagonal chess variants ([De
Vasa's](https://en.wikipedia.org/wiki/Hexagonal_chess#De_Vasa's_hexagonal_chess),
[Brusky's](https://en.wikipedia.org/wiki/Hexagonal_chess#Brusky's_hexagonal_chess),
...).

See below for a visual illustration of how it applies to the standard
Skytear maps:

* 2-lane map

![2-lane map coordinates](maps/2lane.svg "2-lane coordinates")

* 3-lane map

![3-lane map coordinates](maps/3lane.svg "3-lane coordinates")

* Ashen Pass

![Ashen Pass map coordinates](maps/AshenPass.svg "Ashen Pass coordinates")
