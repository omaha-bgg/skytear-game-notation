+ <HERO>.activate
-

.exhaust
.conditions-<CONDITION>-<CONDITION>= {optional}
+ .end_activation
-

+ .move
- .to(<HEX>)

+ .lead
- .choose:<ZONE> {hand or deck}

+ .attack(target=<ENEMY>)
- .flip:<CARD>,<CARD>
  <ENEMY>.HP-<DMG>=<TOTAL>
  minion@<HEX>.remove
  minion@<HEX>.HP-0=1

+ .skirmish
- .to(<HEX>)
  .damage(<HERO>)
  .flip:<CARD>
  <HERO>.HP-<DMG>=<TOTAL>
  .to(<HEX>)

+ <KURUMO>.worship(target=<HERO>)
- <HERO>.conditions+marked
  {trigger(.<MARK_SKILL>)}

+ <LIOTHAN>.worship
  {optional} .exhaust
             .conditions-<CONDITION>-<CONDITION>=
- <HERO>.conditions+shapeshifted

+ <TAULOT>.worship
- .pillar(<HEX>)

+ <TAULOT>.worship
- .pillar@<HEX>.remove
  {trigger(.<SKILL>)}
  .pillar(<HEX>)

+ <NUPTEN>.worship
- .illusion.to(<HEX>)
