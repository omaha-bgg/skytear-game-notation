{Kurumo}


{Akimo.entergame}
{effects+Akimo.Soulthirst}
{triggers+Akimo.WrathOfIshiro}


+ Akimo.attack
  {use_effect(.Soulthirst) damage+1}


+ Akimo.worship(target=<HERO>)
- <HERO>.conditions+marked
  {trigger(.WrathOfIshiro)}
+ .WrathOfIshiro
- .to(<HEX>)


+ Akimo.DemonicPossession(target=<HERO>)
- .conditions+frenzy+fast
  .attack(target=<HERO>) {optional}


{Hogosai.entergame}
{effects+Hogosai.Radiance}
{trigger+Hogosai.PurifyingFire}


{use_effect(Hogosai.Radiance) <HERO>.armor-1}


+ Hogosai.worship(target=<HERO>)
- <HERO>.conditions+marked
  {trigger(.PurifyingFire)}
+ .PurifyingFire
- {optional} .flip:<CARD>
             .HP-<DMG>=<TOTAL>
             <HERO>.HP-<DMG>=<TOTAL>


+ Hogosai.Supernova
- <ENEMY>.HP-<DMG>=<TOTAL> {multiple}


{Kichie.entergame}
{effects+Kichie.Rejuvenation}
{triggers+Kichie.WrittenInFire}


+ Kichie.<ACTION>
- <ENEMY>.HP-<DMG>=<TOTAL>
  Kichie.HP+1=<TOTAL> {use_effect(.Rejuvenation)}


+ Kichie.worship(target=<HERO>)
- <HERO>.conditions+marked
  {trigger(.WrittenInFire)}
+ .WrittenInFire
- .predict(<N>):<CARD>,<CARD>
  .discard(<CARD>) {or to_deck}
  .to_deck(<CARD>) {or discard}


+ Kichie.CurseOfYears(target=<HERO>)
- <HERO>.HP-<DMG>=<TOTAL>
  .HP+<DMG>=<TOTAL> {optional}


{Kumaya.entergame}
{effects+Kumaya.Smokescreen}
{trigger+Kumaya.BurningCensers}


{use_effect(Kumaya.Smokescreen)}
{visible only as explanation for LoS changes}


+ Kumaya.worship(target=<HERO>)
- <HERO>.conditions+marked
  {trigger(.BurningCensers)}
+ .BurningCensers
- .flip:<CARD>
  .HP-<DMG>=<TOTAL> {multiple}


+ Kumaya.EnterTheVoidPalace(target=<HERO>)
- .to(VoidPalace)
  <HERO>.to(VoidPalace)


{Miyuki.entergame}
{effects+Miyuki.SoulKeeper}
{triggers+Miyuki.SpiritOfSorrows}


{use_effect(Miyuki.SoulKeeper) damage-1,flip=0}


+ Miyuki.worship(target=<HERO>)
- <HERO>.conditions+marked
  {trigger(.SpiritOfSorrows)}
+ .SpiritOfSorrows
- <HERO>.conditions+slow


+ Miyuki.CagedSoul
- .choose:<HERO>
  {effects+Miyuki.CagedSoul(<HERO>)}

  <HERO>.HP=0
  <
<HERO>.HP=<MAXHP> {use_effect(CagedSoul(<HERO>)) replaces <HERO>.defeated}
{effects-Miyuki.CagedSoul(<HERO>)}

+ Achla.ComaToxin(<HERO>)
- <HERO>.HP=<MAXHP> {use_state_effect(CagedSoul(<HERO>)) replaces <HERO>.defeated}
{effects-Miyuki.CagedSoul(<HERO>)}

{end of hero phase}
{effects-Miyuki.CagedSoul(<HERO>)}


{Sakoshi.entergame}
{effects+Sakoshi.EagleEye}
{triggers+Sakoshi.ExplosiveShuriken}


{use_effect(Sakoshi.EagleEye)}
{visible only as explanation for LoS changes}


+ Sakoshi.worship(target=<HERO>)
- <HERO>.conditions+marked
  {trigger(.ExplosiveShuriken)}
+ .ExplosiveShuriken
- .flip:<CARD>
  <MINION>.HP-<DMG>=<TOTAL>


+ Sakoshi.ShurikenStorm
- {effects+Sakoshi.ShurikenStorm}

+ Sakoshi.attack(...)
- {use_effect(Sakoshi.ShurikenStorm)}
  <ENEMY>.HP-<DMG>=<TOTAL> {multiple}

+ Sakoshi.end_activation
- {effects-Sakoshi.ShurikenStorm}


{Yami.entergame}
{effects+Yami.DeathlyTouch}
{triggers+Yami.ShadowOfTheReaper}


+ Yami.move
- .to(<HEX>)
  {use_effect(.DeathlyTouch)}
  .flip:<CARD>
  <MINION>.remove {optional}


+ Yami.worship(target=<HERO>)
- <HERO>.conditions+marked
  {trigger(.ShadowOfTheReaper)}
+ .ShadowOfTheReaper
- .to(<HEX>)
  .flip:<CARD>
  <HERO>.HP-<DMG>=<TOTAL>


+ Yami.KneelBeforeMe(target=<HERO>)
- <HERO>.HP-<DMG>=<TOTAL>
  <HERO>.conditions+slow
  <HERO>.to(<HEX>)
  <HERO>.conditions+marked


{Liothan}


{Astryda.entergame}
{effects+Astryda.SongOfLiothan}


+ <HERO>.attack(...)
- <HERO>.HP-<DMG>=<TOTAL>
  {trigger(Astryda.SongOfLiothan)}
+ Astryda.SongOfLiothan
- <HERO>.conditions+disarmed


Astryda.conditions+shapeshifted
{triggers+Astryda.SongOfTheDepths}

+ Astryda.attack(...)
- ...
  <MINION>.remove
  {trigger(Astryda.SongOfTheDepths)}
+ Astryda.SongOfTheDepths
- {optional} .HP-2=<TOTAL>
             .spawn(<HEX>)

Astryda.conditions-shapeshifted
{triggers-Astryda.SongOfTheDepths}


+ Astryda.SongOfTheSiren(target=<HERO>)
- <HERO>.exhaust


{Brylvar.entergame}
{effects+Brylvar.GrialthsProtection}


{use_effect(Brylvar.GrialthsProtection) armor+1}


Brylvar.conditions+shapeshifted
{triggers+Brylvar.ValiantTaunt}

+ <HERO>.activate
  + Brylvar.ValiantTaunt(<HERO>) {trigger}
  - {effects+<HERO>.must_attack(Brylvar)}
-
+ .attack target=Brylvar

Brylvar.conditions-shapeshifted
{triggers-Brylvar.ValiantTaunt}


+ Brylvar.Avalanche
- <HERO>.to(<HEX>)
  .to(<HEX>)
  <HERO>.to(<HEX>)
  .to(<HEX>)
  <HERO>.to(<HEX>)
  .to(<HEX>)


{Corjof.entergame}
{effects+Corjof.Sentinel}


+ <HERO>.skirmish
  {use_effect(Corjof.Sentinel) damage+1}


Corjof.conditions+shapeshifted
{effects+Corjof.ShapeOfTheClawed}

+ Corjof.activate
  + .ShapeOfTheClawed
  - .conditions+fast
-

{use_effect(Corjof.ShapeOfTheClawed) attack+1 melee}

Corjof.conditions-shapeshifted
{effects-Corjof.ShapeOfTheClawed}


+ Corjof.FeatherStorm
- .to(<HEX>)
  {optional} <HERO>.HP-<DMG>=<TOTAL>
             <HERO>.conditions+disarmed


{Freyhel.entergame}
{triggers+Freyhel.QueenOfLife}


  <HERO>.conditions+shapeshifted
  {trigger(Freyhel.QueenOfLife)}
+ Freyel.QueenOfLife
- {optional} .flip:<CARD>
             <HERO>.HP+<N>=<TOTAL>


Freyel.conditions+shapeshifted
{effects+Freyhel.MistressOfWithering.1}
{effects+Freyhel.MistressOfWithering.2}

{use_effect(Freyel.MistressOfWithering.1) piercing}

<HERO>.HP-<N>=<TOTAL> {use_effect(Freyel.MistressOfWithering.2) replaces <HERO>.HP+<N>=<TOTAL>}

Freyel.conditions-shapeshifted
{effects-Freyhel.MistressOfWithering}


+ Freyel.GlacialRebirth
- <HERO>.HP+3=<TOTAL> {multiple}
  <HERO>.HP-3=<TOTAL> {multiple optional}


{Gullbjarn.entergame}
{effects+Gullbjarn.FeralMajesty}


+ Gullbjarn.skirmish
  {use_effect(.FeralMajesty) damage+<N>}


Gullbjarn.conditions+shapeshifted
{effects+Gullbjarn.UrsineRage}

+ Gullbjarn.skirmish
- .choose:<AOE> {use_effect(.UrsineRage) replaces .choose:<HERO>}
  .flip:<CARD>
  <HERO>.HP-<N>=<TOTAL> {multiple}

Gullbjarn.conditions-shapeshifted
{effects-Gullbjarn.UrsineRage}


+ Gullbjarn.FeralVitality
- Gullbjarn.conditions=
  Gullbjarn.HP=<MAXHP>
  <HERO>.to(<HEX>) {optional multiple}


{Shyllavi.entergame}
{triggers+Shyllavi.Revenge}


  <HERO>.HP-<DMG>=<TOTAL>
  {trigger(Shyllavi.Revenge)}

+ Shyllavi.Revenge(target=<HERO>)
- {optional} {<HERO>.damage(<HERO>)}
             .flip:<CARD>
             <HERO>.HP-<N>=<TOTAL>


Shyllavi.conditions+shapeshifted
{triggers+Shyllavi.FelinePounce}

+ Shyllavi.attack(target=<MINION>)
- .flip:<CARD>
  <MINION>.remove
  {trigger(.FelinePounce)}
+ .FelinePounce(target=<MINION>)
- .flip:<CARD>
  <MINION>.HP-<N>=<TOTAL>

Shyllavi.conditions-shapeshifted
{triggers-Shyllavi.FelinePounce}


+ Shyllavi.DanceOfThePredator
- .to(<HEX>)
  .damage(<ENEMY>)
  .flip:<CARD>
  <ENEMY>.HP-<N>=<TOTAL>
  {optional} .to(<HEX>)
             .damage(<ENEMY>)
             .flip:<CARD>
             <ENEMY>.HP-<N>=<TOTAL>


{Vorhild.entergame}
{triggers+Vorhild.OneThousandTinyFangs}


  .pillar(<HEX>)
  {trigger(Vorhild.OneThousandTinyFangs)}

  pillar@<HEX>.remove
  {trigger(Vorhild.OneThousandTinyFangs)}

+ Vorhild.OneThousandTinyFangs(target=<HERO>)
- <HERO>.HP-<N>=<TOTAL>


Vorhild.conditions+shapeshifted
{effects+Nimble.1 passive effect do not show in notation}
{triggers+Vorhild.Nimble.2}

+ Vorhild.activate {conditions=shapeshifted}
  + .Nimble.2 {trigger}
  - .conditions-shapeshifted

Vorhild.conditions-shapeshifted
{effects-Nimble.1}
{triggers-Vorhild.Nimble.2}


+ Vorhild.TheErminesEye(target=<HEX>)
- .flip
  <HERO>.HP-<DMG>=<TOTAL>
  <HERO>.conditions+slow+disarmed


{Nupten}


{Akhuti.entergame}
{triggers+Akhuti.Retribution}
{effects+Akhuti.ResonantLight projected armor+1}


+ Akhuti.attack(...)
- damage(<MINION>) {optional}


+ Akhuti.reaction_WordOfNupten
- <HERO>.HP+<N>=<TOTAL> {multiple}
  <HERO>.conditions+disarmed {multiple}


{Ekhrit.entergame}
{effects+Ekhrit.WindBarrier}
{effects+Ekhrit.LadyOfGales} {visible only as explanation for prevention of effects}


+ Ekhrit.attack {use_effect(Ekhrit.WindBarrier)}
- {multiple} <HERO>.HP-<DMG>=<TOTAL>
             <HERO>.to(<HEX>)


+ Ekhrit.reaction_EyeOfTheStorm
- {effects+Ekhrit.EyeOfTheStorm} {visible only as explanation for prevention of effects}
  {multiple} <HERO>.HP-<DMG>=<TOTAL>
             <HERO>.to(<HEX>)


{Haburat.entergame}
{effects+Haburat.entergame attack+1}
{triggers+Haburat.WayOfTheRogue}


+ <HERO>.attack(<MINION>)
- .flip
  <MINION>.HP-<DMG>=<TOTAL>
  {trigger(Haburat.WayOfTheRogue(<HERO>))}
+ Haburat.WayOfTheRogue(<HERO>)
- <HERO>.to(<HEX>)


+ Haburat.BladesOfLightning
- {triggers+Haburat.BladesOfLightning}

+ Haburat.attack
  + Haburat.BladesOfLightning {trigger}
  - .flip:<CARD>
    <ENEMY>.HP-<N>=<TOTAL> {multiple}
- ...


{Khenui.entergame}
{effects+Khenui.Echoed} {visible only as explanation for prevention of effects}
{triggers+Khenui.AcrobaticEscape}


  <HERO>.HP-<N>=<TOTAL>
  {trigger(Khenui.AcrobaticEscape(<HERO>))}
+ Khenui.AcrobaticEscape(<HERO>)
- <HERO>.to(<HEX>)


+ Khenui.action_EchoDance
- <HERO>.illusion.to(<HEX>) {multiple}
  <HERO>.to(<HEX>) {multiple}


{Setheru.entergame}
{effects+Setheru.MasterOfIllusions powerdamage+1}
{effects+Setheru.LordOfSkytear range modifier}


+ Setheru.action_LastJudgement(<HEX>)
- .choose:<AOE>
  <HERO>.HP-<N>=<TOTAL> {multiple}


{Shafathi.entergame}
{triggers+Shafathi.TimeOverlap}
{triggers+Shafathi.SchemersProtection}


+ Shafathi.skirmish
- {trigger(.TimeOverlap)}
  ...
+ .TimeOverlap
- .swap(.illusion) {optional}


+ Shafathi.skirmish
  + .SchemersProtection {trigger}
  - .predict(1)
-


+ .end_activation
  + Shafathi.Flashback
  - Shafathi.ready


{Shaidrus.entergame}
{triggers+Shaidrus.Momentum}
{effect+Shaidrus.EverpresentCustodian}


Shaidrus.exhaust
{trigger(Shaidrus.Momentum)}
+ Shaidrus.Momentum
- {optional} <HERO>.to(<HEX>)
             <HERO>.conditions+slow


  Shaidrus.HP-<N>=<TOTAL> {use_effect(Shaidrus.EverpresentCustodiann) replace <HERO>.HP-<N>=<TOTAL>}


{Taulot}


{Achla.entergame}
{effect+Achla.NowhereToHide}


+ Achla.worship
- <PILLAR>.remove
  flip:<CARD>
  <HERO>.HP-<N>=<TOTAL>


+ Achla.action_ComaToxin(<HERO>)
- <HERO>.defeated


{Cotlic.entergame}
{effect+Cotlic.TaulotsBlessing}


+ Cotlic.worship
- <PILLAR>.remove {multiple}
  {effect+Cotlic.StrengthOfTheKing skirmish+<N>}


+ Cotlic.action_MyKingdom
- <PILLAR>.remove
  .pillar(<HEX>)
  .pillar(<HEX>) {optional}
  <MINION>.defeat {multiple}


{Ixatosk.entergame}
{triggers+Ixatosk.UnstableGround}


+ <HERO>.activate
  + Ixatosk.UnstableGround {trigger}
  - <HERO>.conditions+slow
-


+ Ixatosk.worship
- <PILLAR>.remove
  .to(<HEX>)
  <HERO>.to(<HEX>) {optional}


+ Ixatosk.action_FromBelow
- .to(<HEX>)
  .spawn(<HEX>) {optional}
  .spawn(<HEX>) {optional}


{Nantaca.entergame}
{effects+Nantaca.RulerOfTheLivingAndDead}
{effects+Nantaca.NatureIncarnate.1}


+ Nantaca.attack(...)
- {use_effect(.RulerOfTheLivingAndDead) damage+<N>}


+ <LIOTHAN>.worship
- Nantaca.conditions+shapeshifted {use_effect(Nantaca.NatureIncarnate.1)}
  {effects+Nantaca.NatureIncarnate.2 cannot be moved}


+ Nantaca.action_CallOfTheQueen
- .conditions+frenzy
  {effects+Nantaca.CallOfTheQueen}

+ Nantaca.attack(...)
- damage(<HERO>) {multiple}


{Neclalen.entergame}
{effects+Neclalen.ArchaicMastery}


+ Neclalen.<CARD> {use_effect(Neclalen.ArchaicMastery)}


+ Neclalen.worship
- <PILLAR>.remove
  .discard(<CARD>) {multiple}
  {effects+Neclalen.TheHeartClaims(<HEX>)}


+ Neclalen.<CARD> {use_effect(Neclalen.TheHeartClaims(<HEX>))}


+ Neclalen.action_ConsultingTheHeart
- .search_draw:<CARD>
  .<CARD> {optional}
  .shuffle


{Tlakali.entergame}
{effects+Tlakali.ProtectiveRite}


  Tlakali.HP-<N>=<TOTAL> {use_effect(Tlakali.ProtectiveRite) replace <MINION>.HP-<N>=<TOTAL>}


+ Tlakali.worship
- <PILLAR>.remove
  .flip:<CARD>
  <HERO>.HP+<N>=<TOTAL>


+ Tlakali.action_TheThousandthRitual
- <HERO>.conditions+<CONDITION> {multiple}


{Zacoal.entergame}
{triggers+Zacoal.MassiveBlow}


+ Zacoal.attack(<HERO>)
- ...
  <HERO>.HP-<N>=<TOTAL>
  <HERO>.to(<HEX>) {optional}


+ Zacoal.worship
- <PILLAR>.remove
  .flip:<CARD>
  <MINION>>.HP-<N>=<TOTAL>


+ Zacoal.reaction.TotemicSlam(<HERO>)
- <HERO>.HP-<N>=<TOTAL>
  <HERO>.conditions+slow
  <HERO>.to(<HEX>)
  {optional} <HERO>.flip:<CARD>
             <HERO>.HP-<N>=<TOTAL>
