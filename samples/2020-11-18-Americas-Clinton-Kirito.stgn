---
Event: Skytear Americas Regional Championships 3rd Place match
Date: 2020.11.18
Recording: https://www.youtube.com/watch?v=qbRsVIPDYrc

Rules: Living Rules 2020-10-06
Map: Ashen Pass
Sets:
  - Starter Box
  - Kurumo
  - Liothan
  - Nupten
  - Into Ashes

North: Clinton
South: rlcordan aka Kirito(Ryan)
Decks:
  N:
    Name: Flip Ya For Real
    Heroes:
      Setheru:
        Vampirism:
        SkytearDischarge:
        ClearMind:
        Doom:
      Yami:
        PsychicShield:
        Doom:
        TimeGlitch:
      Hogosai:
        Impervious:
        QuickShot:
        ShieldSlam:
      Corjof:
      Kichie:
        SkytearDischarge:
        ClearMind:
        Unstoppable:
      Akhuti:
    Outsider: DarkVigilante
  S:
    Name: Suicide Squad
    # precise deck version is unsure
    maybe-url: https://community.playskytear.com/decks/ESaXsG6x76Hps2eJJXsP ?
      https://community.playskytear.com/decks/GjlAAfulSdkZJEMFwydb ?
    Heroes:
      Shyllavi:
      Freyhel:
      Brylvar:
      Astryda:
      Gullbjarn:
      Ekhrit:
    Outsider: Outsider
Objectives:
  - Onslaught
  - TheTamer
---

1.0. {game setup }

coin toss: North
North choice: play first

N.Kichie.to(f9)
S.Shyllavi.to(e2)
S.Freyhel.to(d1)
N.Yami.to(e8)
N.Hogosai.to(d8)
S.Astryda.to(f2)
S.Gullbjarn.to(reserve)
N.Setheru.to(reserve)

N.Kichie.HP=14
N.Yami.HP=18
N.Hogosai.HP=17
N.Setheru.HP=14
S.Shyllavi.HP=18
S.Freyhel.HP=16
S.Astryda.HP=16
S.Gullbjarn.HP=18

N.draw(6) {:unknown}
S.draw(6) {:unknown}
N.mulligan(2):Impervious {2:18},PsychicShield{2:25}
S.mulligan(3):GlacialRebirth{2:33},RampantHatred{2:36},RampantHatred {2:58}

turn(1)

{moves}

1.1.
+ N.Yami.activate {HP=18,conditions=}
  .AP=3
-

+ .move {3:16}
  .AP-1=2
- .to(e5)

+ .worship(target=S.Shyllavi)
  .AP-1=1
- S.Shyllavi.conditions+marked
  {trigger(.ShadowOfTheReaper)}
+ .ShadowOfTheReaper
- .to(f3)
  N.flip:SkytearDischarge{+2}
  S.Shyllavi.HP-3=15

+ .skirmish
  .AP-1=0
- .damage(S.Shyllavi)
  .flip:TimeGlitch{+2}
  S.Shyllavi.HP-2=13
  .to(h5)

.exhaust
+ .end_activation
-

1.2.
+ S.Shyllavi.activate {HP=13,conditions=marked}
  .AP=3
-

+ .worship {4:11}
  .AP-1=2
- S.Astryda.conditions+shapeshifted
  {triggers+S.Astryda.SongOfTheDepths}

+ .move {4:20}
  .AP-1=1
- .to(b1)

+ .lead {4:36}
  .AP-1=0
- .choose:deck {revealed FeralVitality(3) at 12:41}

.exhaust
.conditions-marked=
+ .end_activation
-

1.3.
+ N.Hogosai.activate {HP=17,conditions=}
  .AP=3
-

+ .move {5:25}
  .AP-1=2
- .to(g8)

+ .skirmish
  .AP-1=1
- .to(h7)

+ .lead {5:47}
  .AP-1=0
- .choose:hand {revealed Last Judgment(3) at 12:56}

.exhaust
+ .end_activation
-

1.4.
+ S.Astryda.activate {HP=16,conditions=shapeshifted}
  .AP=3
-

+ .move {6:05}
  .AP-1=2
- .to(c2)

+ .attack(target=N.minion@d5) {6:08}
  .AP-1=1
- .flip:Sinkhole{+0}
  N.minion@d5.remove
  {trigger(.SongOfTheDepths)}
+ .SongOfTheDepths
- .HP-2=14
  .spawn(e5)

+ .skirmish {6:26}
  .AP-1=0
  + N.Kichie.reaction_SkytearDischarge
  - {triggers+N.Kichie.SkytearDischarge}
- .to(b3)
  trigger(N.Kichie.SkytearDischarge)
+ N.Kichie.SkytearDischarge
- N.flip:PsychicShield{+3} {7:18}
  .HP-5=9

.exhaust
.conditions-shapeshifted=
{triggers-S.Astryda.SongOfTheDepths}
+ .end_activation
- {triggers-N.Kichie.reaction_SkytearDischarge}

1.5.
+ N.Kichie.activate {HP=14,conditions=}
  .AP=3
-

+ .move {9:18}
  .AP-1=2
- .to(c6)

+ .skirmish
  .AP-1=1
- .to(b5)
  .damage(S.Astryda)
  .flip:ShieldSlam{+2}
  S.Astryda.HP-2=7
  .to(c6)

+ .attack(target=S.minion@e5)
  .AP-1=0
- .flip:ClearMind{+2}
  S.minion@e5.remove

.exhaust
+ .end_activation
-

1.6.
+ S.Freyhel.activate {HP=16,conditions=}
  .AP=3
-

+ .move {11:00}
  .AP-1=2
- .to(c3)

+ .attack(target=N.minion@e6)
  .AP-1=1
- S.flip:Chasm{+0}
  N.minion@e6.remove

+ .skirmish
  .AP-1=0
- .to(b2)
+ .reaction_Sacrifice
- .HP-2=14
  S.Astryda.HP+4=11
+ S.Shyllavi.reaction_Nourish {11:41}
- S.Astryda.HP+3=14
+ S.Astryda.reaction_Nourish {11:50}
- S.Shyllavi.HP+3=16

.exhaust
+ .end_activation
-

1.7. {lane 1}
{tally: N: 1 hero + 1 minion, S: 3 minions, S wins +1}
N.minion@f6.remove
token@e5.to(e6)
S.spawn(e6)
S.spawn(d5)
N.spawn(e6)
N.spawn(f6)

1.8. {dome 2}
S.Shyllavi.reveal_lead:FeralVitality{3}
{tally: S: 3 heroes, 3 lead, S wins +6}
S.Outsider.HP=20
S.Outsider.to(b4,b5,c5)

+ S.Outsider.activate
  .AP=3
-

+ .DivineConduit(target=S.Freyhel) {15:00}
  .AP-1=2
- S.Freyhel.worship
  S.Freyhel.conditions+shapeshifted
  {effects+Freyhel.MistressOfWithering.1}
  {effects+Freyhel.MistressOfWithering.2}

+ .skirmish
  .AP-1=1
- .damage(N.Kichie) {15:02}
  S.flip:StrengthOverPain{-1}

+ .attack(target=N.Kichie) {15:13}
  .AP-1=0
- .flip:Sinkhole{+0}
  N.Kichie.HP-3=11

+ .end_activation
-

1.9. {dome 3}
N.Hogosai.reveal_lead:LastJudgment{3}
{tally: N: 2 heroes, 3 lead, N wins +5}
N.DarkVigilante.HP=19
N.DarkVigilante.to(e8)

+ N.DarkVigilante.activate
  .AP=3
-

+ .Slash
  .AP-1=2
- {effects+.Slash}
  .skirmish
  .to(d6)
  .damage(S.minion@d5) {use_effect(.Slash)}
  .flip:ShieldSlam{+2}
  S.minion@d5.remove

+ .Sever
  .AP-1=1
- {effects+.Sever}
  .skirmish
  .to(d5)
  .damage(S.minion@d4) {use_effect(.Slash)}
  .flip:Doom{+2} {16:10}
  S.minion@d4.remove

+ .skirmish
  .AP-1=0
- .to(c3) {17:45}
  .damage(S.minion@e4) {use_effect(.Slash)}
  .flip:ClearMind{+2} {17:05}
  S.minion@e4.remove

+ .end_activation
- {effects-.Slash}
  {effects-.Sever}

2.0. {17:49 end of round}
N.Hogosai.discard(LastJudgment)
S.Shyllavi.discard(FeralVitality)
N.Kichie.discard(SkytearDischarge)
{FIXME TTS shows Nourish/Nourish/Sacrifice ?}
S.Freyhel.discard(Sacrifice)
S.Shyllavi.discard(Nourish)
S.Astryda.discard(Nourish)

S.draw(2) {17:57}
N.draw(2)

N.Kichie.ready
N.Yami.ready
N.Hogosai.ready
S.Shyllavi.ready
S.Freyhel.ready
S.Astryda.ready

turn(2)

2.1.
+ N.Hogosai.activate {HP=17,conditions=}
  .AP=3
-

+ .move {18:54}
  .AP-1=2
- .to(e5)

+ .attack(target=minion@e6) {19:35}
  .AP-1=1
- .flip:NightmaresIncarnate{+1}
  S.minion@e6.remove

+ .worship(target=S.Astryda) {20:09}
  .AP-1=0
- S.Astryda.conditions+marked
  {trigger(.PurifyingFire)}
+ .PurifyingFire
- .flip:TimeGlitch{+2}
  .HP-3=14
  S.Astryda.HP-3=11

.exhaust
+ .end_activation
-

2.2.
+ S.Shyllavi.activate {HP=16,conditions=}
  .AP=3
-

+ .action_GatheringStorm
- .draw(2) {7 in hand}

+ .worship {21:26}
  .AP-1=2
- S.Astryda.conditions+shapeshifted
  {triggers+Astryda.SongOfTheDepths}
  {trigger(S.Freyhel.QueenOfLife)}
+ S.Freyhel.QueenOfLife
- .flip:Nourish{+1}
  S.Astryda.HP+2=13

+ .skirmish
  .AP-1=1
- .to(c2)
  .damage(DarkVigilante)
  .flip:ThroughTheEyes{+2}
  DarkVigilante.HP-1=18
  .to(b1)

+ .lead {22:00}
  .AP-1=0
- choose:deck {revealed ?{3} at 12:41}

.exhaust
+ .end_activation
-

2.3.
+ N.Yami.activate {HP=18,conditions=}
  .AP=3
-

+ .move {22:48}
  .AP-1=2
- .to(e4)
  {:DeathlyTouch:S.minion@f6}
  N.flip:Unstoppable{+1}
  S.minion@f6.remove

+ .worship(target=S.Astryda)
  .AP-1=1
- .flip:Unstoppable{+1}
  S.Astryda.HP-1=12
  .to(c4)

+ .lead {24:04}
  .AP-1=0
- choose:deck {revealed ?}

.exhaust
+ .end_activation
-

2.4.
+ S.Freyhel.activate {HP=14,conditions=shapeshifted}
  .AP=3
-

+ .action_GatheringStorm {26:29}
- .draw(2) {8 in hand}

+ .move {27:43}
  .AP-1=2
  + S.Astryda.reaction_SkytearDischarge
    + S.Astryda.reaction_SkytearDischarge
    - {triggers+S.Astryda.reaction_SkytearDischarge}
  - {triggers+S.Astryda.reaction_SkytearDischarge}
- .to(c2)
  {trigger(S.Astryda:SkytearDischarge)}
  {trigger(S.Astryda:SkytearDischarge)}
  .to(b2)
  {trigger(S.Astryda:SkytearDischarge)}
  {trigger(S.Astryda:SkytearDischarge)}
  .to(c2)
  {trigger(S.Astryda:SkytearDischarge)}
  {trigger(S.Astryda:SkytearDischarge)}
+ S.Astryda:SkytearDischarge
  + S.Astryda:SkytearDischarge
    + S.Astryda:SkytearDischarge
      + S.Astryda:SkytearDischarge
        + S.Astryda:SkytearDischarge
          + S.Astryda:SkytearDischarge
            + N.Yami.reaction_Doom(target=S.Freyhel) {29:05}
            - S.Freyhel.manaburn:? {FIXME: not seen?}
              {effects+N.Yami.reaction_Doom S.Freyhel no heal}
          - S.flip:Sacrifice{+1}
            .HP-2=12
            {trigger(S.Shyllavi.Revenge)}
          + S.Shyllavi.Revenge(target=DarkVigilante)
          - {.damage(DarkVigilante)}
            S.flip:SkytearDischarge{+2}
            DarkVigilante.HP-3=15
        - S.flip:DanceOfThePredator{+3} {30:28}
          .HP-4=8
          {trigger(S.Shyllavi.Revenge)}
        + S.Shyllavi.Revenge(target=DarkVigilante)
        - S.flip:StrengthOverPain{-1}
      - S.flip:StrengthOverPain{-1}
    - S.flip:Sacrifice{+1} {31:15}
      .HP-2=6
      {trigger(S.Shyllavi.Revenge)}
    + S.Shyllavi.Revenge(target=DarkVigilante)
    - S.flip:RampantHatred{+1} {31:25}
      DarkVigilante.HP-2=13
  - S.flip:Chasm{+0} {31:41}
    .HP-1=5
    {trigger(S.Shyllavi.Revenge)}
  + S.Shyllavi.Revenge(target=DarkVigilante)
  - S.flip:RampantHatred{+1} {32:05}
    DarkVigilante.HP-2=11
- S.flip:RampantHatred{+1} {32:18}
  .HP-2=3
  {trigger(S.Shyllavi.Revenge)
+ S.Shyllavi.Revenge(target=DarkVigilante)
- S.flip:FeralVitality{+3} {32:26}
  DarkVigilante.HP-5=6 {FIXME why not -4 ?}

+ .worship {34:41}
  .AP-1=1
- S.Shyllavi.conditions+shapeshifted {34:51}
  {triggers+Shyllavi.FelinePounce}
  {trigger(.QueenOfLife)}
+ .QueenOfLife
- S.flip:Sinkhole{+0}
  S.Shyllavi.HP+1=17

+ .skirmish
  .AP-1=0
- .damage(N.Hogosai)
  .to(b2)
  {trigger(S.Astryda.SkytearDischarge)}
  {trigger(S.Astryda.SkytearDischarge)}
  .to(c2)
  {trigger(S.Astryda.SkytearDischarge)}
  {trigger(S.Astryda.SkytearDischarge)}
+ S.Astryda.SkytearDischarge
  + S.Astryda.SkytearDischarge
    + S.Astryda.SkytearDischarge
      + S.Astryda.SkytearDischarge
      - S.flip:ThroughTheEyes{+2} {35:50}
        .HP-3=0
        <
      .defeated {FIXME not allowed to do anything here ?}
      {trigger(.end_activation)}
      N.objectives.Onslaught+1=1
      {trigger(S.Gullbjarn.ready)}
      + .end_activation {FIXME supposed to be pushed after resolution ends}
        .conditions-shapeshifted=
        {effects-.MistressOfWithering.1}
        {effects-.MistressOfWithering.2}
        {triggers-S.Astryda.reaction_SkytearDischarge}
        {triggers-S.Astryda.reaction_SkytearDischarge}
        {effects-N.Yami.reaction_Doom}
        {FIXME discard?}
      -
      + S.Gullbjarn.ready {37:09}
      - S.Gullbjarn.to(e2)
    -
  -
-

2.5.
+ N.Kichie.activate {HP=11,conditions=}
  .AP=3
-

+ .worship {38:04}
  .AP-1=2
- DarkVigilante.conditions+marked
  {trigger(.WrittenInFire)}
+ .WrittenInFire
- .predict(2)
  .to_deck(?)
  .to_deck(?)

+ .attack(target.DarkVigilante) {39:52}
  .AP-1=1
- N.flip:Supernova{+3}
  DarkVigilante.HP-6=0
  DarkVigilante.remove
  N.objectives.Onslaught+1=2 {40:02}

+ .lead
  .AP-1=0
- .choose:hand

.exhaust
+ .end_activation
-

2.6.
+ S.Astryda.activate {HP=12,conditions=marked,shapeshifted} {43:02}
  .AP=3
-

+ .action_GatheringStorm {43:33}
- .draw(2) {7 in hand}

+ .skirmish {43:44}
  .AP-1=2
  + N.Kichie.reaction_SkytearDischarge
  - {triggers+N.Kichie.reaction_SkytearDischarge}
- .to(c3)
  {trigger(N.Kichie.SkytearDischarge)}
  .damage(N.Yami)
  .flip:Sacrifice{+1} {44:12} {no HP loss}
+ N.Kichie.SkytearDischarge
- N.flip:NightmaresIncarnate{+1} {44:17}
  .HP-3=9
  {trigger(S.Shyllavi.Revenge)}
+ S.Shyllavi.Revenge
- {.damage(N.Yami)}
  .flip:StrengthOverPain{-1} {no HP loss}

+ .attack(target=N.minion@e6) {44:43}
  .AP-1=1
- .flip:Nourish{+1}
  N.minion@e6.remove
  {trigger(.SongOfTheDepths)}
+ .SongOfTheDepths
- .HP-2=7
  .spawn(e6)

+ .worship {45:20}
  .AP-1=0
  .exhaust {45:22}
  .conditions-marked-shapeshifted=
  {triggers-.SongOfTheDepths}
  + N.Yami.reaction_Doom(target=S.Astryda) {45:29}
  - S.Astryda.manaburn:reaction_Nourish{1} {45:42}
    {effects+N.Yami.reaction_Doom S.Astryda no heal}
- .conditions+shapeshifted {45:59}
  {triggers+.SongOfTheDepths}

+ .end_activation
- {triggers-N.Kichie.reaction_SkytearDischarge}
  {triggers-N.Yami.reaction_SkytearDischarge}
  {triggers-N.Kichie.reaction_SkytearDischarge}
  {effects-N.Yami.reaction_Doom}

2.7
+ S.Gullbjarn.activate {HP=18,conditions=} {46:07}
  .AP=3
-

+ .skirmish {47:40}
  .AP-1=2
  {use_effect(.FeralMajesty) :damage+2}
- .to(e4)
  .damage(N.Hogosai)
  .flip:Sinkhole(0)

+ .attack(target=N.Hogosai) {47:57}
  .AP-1=1
- .flip:Chasm{+0}
  N.Hogosai.HP-1=13
+ .reaction_ThroughTheEyes {48:19}
- N.Hogosai.conditions+disarmed

+ .lead
  .AP-1=0
- .choose:hand
+ N.Kichie.reaction_NightmaresIncarnate {49:13}
- S.Astryda.HP-3=4

+ .end_activation
-

2.8 {lane 1}
S.Gullbjarn.reveal_lead:{2}
N.Yami.reveal_lead:{1}
N.Kichie.reveal_lead:{3}
{tally N: 3 heroes, 1 minion, 3 lead, S: 2 heroes, 1 minion, 2 lead, N wins +2}
S.minion@e6.remove
token@e6.to(e4) {49:53}
N.spawn(f5)
N.spawn(f4)
S.spawn(e3)
S.spawn(f4)

2.9 {dome 2}
S.Shyllavi.reveal_lead:{1} {50:37 revealed as Redirect at 51:59}
{tally S: 1 hero + 1 lead, S wins +2}
S:Outsider.to(b4,b5,c5)

+ S:Outsider.activate
  .AP=3
-

+ .DivineConduit(target=S.Gullbjarn) {FIXME: or is it ?}
  .AP-1=2
- S.Gullbjarn.worship
  S.Gullbjarn.conditions+shapeshifted {50:34}
  {effects+Gullbjarn.UrsineRage}

+ .attack(target=N.Kichie)
  .AP-1=1
- S.flip:Nourish{+1}
  N.Kichie.HP-4=7

+ .Shockwave(target=N.Kichie)
  .AP-1=0
- S.flip:Sacrifice{+1} {51:46}
  N.Kichie.HP-1=6
  N.Kichie.to(d8)

3.0. {51:59 end of round}

{ FIXME discards not fully analyzed
S.Shyllavi.discard(Redirect{+1})
S.Gullbjarn.discard(ThroughTheEyes{+2})

N:Unstoppable{+1} lead?
N.Yami.discard(Doom{+2})
N.Kichie.discard(NightmaresIncarnate{+1})

S.Shyllavi.discard(ThroughTheEyes)
S.Astryda.discard(ThroughTheEyes)
S.Shyllavi.discard(Gathering Storm)
S.Shyllavi.discard(SkytearDischarge)
S.Shyllavi.discard(SkytearDischarge)

N:SkytearDischarge{+2}
N.Yami.discard(Doom{+2})
}

S.draw(2) {52:13}
N.draw(2)

S.discard(SkytearDischarge) {52:19}
N.discard(QuickShot,PsychicVampirism) {52:26}

N.Kichie.ready
N.Yami.ready
N.Hogosai.ready
S.Shyllavi.ready
S.Astryda.ready
S.Gullbjarn.ready

turn(3) {53:19}

3.1.

+ N.Kichie.activate {53:08}
  .AP=3
-

+ .skirmish {52:27}
  .AP-1=2
- .to(d6)
  .damage(S.Astryda)
  N.flip:TimeGlitch{+2}
  S.Astryda.HP-2=2

+ .attack(target=S.Astryda) {53:51}
  .AP-1=1
  + S.Gullbjarn.reaction_Sinkhole(target:d5) {54:18}
    + N.Yami.reaction_KneelBeforeMe(target:S.Astryda) {54:23}
      + S.Astryda.reaction_Redirect(N.Yami.reaction_KneelBeforeMe, N.Yami) {54:49}
      - replace(N.Yami.reaction_KneelBeforeMe.target,Outsider)
    - Outsider.HP-2=18
      Outsider.conditions+slow {FIXME: not +marked ?}
  - .choose:d5,b3 {56:22}
    S.Astryda.to(b3)
    N.Yami.to(c3)
-

+ .worship(target=S.Gullbjarn)
  .AP-1=0
- S.Gullbjarn.conditions+marked {57:22}
  .predict(2)
  {FIXME: is this what hapenned?}
  N.discard(PsychicVampirism{+2})
  N.discard(Unstoppable{+1})

+ .action_CurseOfYears(target=S.Gullbjarn) {57:58}
- S.Gullbjarn.HP-9=9
  .HP+9=14 {15 capped by max}

.exhaust {58:30}
+ .end_activation
-

3.2.

+ S.Gullbjarn.activate {HP=9,conditions=shapeshifted+marked}
  .AP=3
-

+ .skirmish {59:08}
  .AP-1=2
  {use_effect(.FeralMajesty) :damage+3}
- .to(c2)
  .damage(N.Yami)
  S.flip:Sacrifice{+1}
  N.Yami.HP-3=15

+ .attack(target:N.Yami) {59:45}
  .AP-1=1
- S.flip:RampantHatred{+1}
  N.Yami.HP-3=12
  {trigger(S.Astryda.SongOfLiothan)}
+ S.Astryda.SongOfLiothan
- N.Yami.conditions+disarmed

+ .move {1:01:12}
  .AP-1=0
- .to(b2)

.exhaust {1:01:19}
.conditions-shapeshifted-marked=
{effects-Gullbjarn.UrsineRage}
+ .end_activation
-

3.3.

+ N.Yami.activate
  .AP=3
  + .reaction_ClearMind {1:01:31}
  - .conditions-disarmed
    {effects+.reaction_ClearMind .attack+1}
-

+ .worship(target=S.Astryda) {1:01:38}
  .AP-1=2
  + S.Shyllavi.reaction_DanceOfThePredator {1:01:44}
    + N.Hogosai.reaction_Impervious(target=N.Yami) {1:02:19}
    - {N.Yami:armour+2}
  - S.Shyllavi.to(e4)
    S.damage(N.minion@f5)
    S.flip:Through The Eyes{+2}
    N.minion@f5.remove
    S.Shyllavi.to(d5)
    S.damage(N.Kichie)
    S.flip:RampantHatred{+1} {1:03:05}
    N.Kichie.HP-3=11
    {triggers Astryda:SongOfLiothan, just not needed :)}
- S.Astryda.conditions+marked
  {trigger(.ShadowOfTheReaper)}
+ .ShadowOfTheReaper
- .to(a3)
  .flip:TimeGlitch{+2}
  S.Astryda.HP-2=0
  <
S.Astryda.defeated
  N.objectives.Onslaught+1=3 {1:03:23}

{North wins by Onslaught}
