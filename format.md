# Proposed format for the Skytear game notation

[[_TOC_]]

## basics structure

All the syntactic elements described in this section relate to all 5
information levels.

- this is a text-based human-readable format

- it starts with an initial list of metadata fields describing the
  match.  This metadata is enclosed between two lines containing
  solely the `---` characters, and is described in details in next
  section. This metadata include information about the map, the
  players, which decks they brought, and so on.

- then come the list of moves that make up the game.
  `<ROUND>.<ACTIVATION>.` markers are interspersed to help in reading
  and cross-referencing (but a parser may ignore them)

- the moves in the game end up resembling imperative statements in a
  object-oriented programming language.  This is probably not a
  coindidence, as we are describing at each step how to change the
  game elements in order to reach the next state.

- a statement cannot include any whitespace

- the amount of consecutive whitespace between statements is not
  significant

- comments are enclosed in curly brackets (I used them to keep track
  of timestamps in the video stream, and various annotations like lead
  cards revealed later on)

- resolution of lanes and domes in minions phase, as well as game
  setup and end of round are handled with a syntax similar to an
  activation:
  - game setup and end of round get a `<ROUND>.0.` marker (end of turn
    really has many similarities to game setup in preparing the next
    round)
  - lanes and domes are numbered like additional activations
  - tallying the lane for winner determination in a lane or dome
    appears in optional comment
  - dome resolution then looks like a hero respawn and activation
  - lane resolution contains statements such as `MINION.remove`, `spawn`,
    `token.to`, tower and nexus damaging
  - game setup and end of round contain statements like hero spawning
    and readying, card drawing and discarding

## metadata

Teams are identified by the side of the board where their nexus lies,
as _North_ and _South_ (refered to as _side_ in this document) as
defined [in the annotated maps](hex-coordinates.md).  The game
metadata spells this name in full, other uses are abbreviated for
conciseness as _N_ and _S_:

    North: Clinton
    South: rlcordan aka Kirito(Ryan)

The metadata is described using [YAML
syntax](https://en.wikipedia.org/wiki/YAML), which allows to concisely
describe structured textual information.  The purpose of this sections
is twofold:

- providing information valuable for indexing a collection of games,
  and possibly make some statistics about them
- setting up the scope of the game description (map, decks brought by
  players

It contains a set of named fields.  Simple fields describing the event
would be:

    Event: Skytear Americas Regional Championships 3rd Place match
    Date: 2020.11.18
    Recording: https://www.youtube.com/watch?v=qbRsVIPDYrc

Fields specified in this notation will always start with a capital
letter.  Additional fields starting with a lower-case letter can be
used at will.

**FIXME** YAML appears to have some optional parts, we should define
the subset we need.

YAML allows to use comments using the `#` character.

Some fields describe the choice of map, the Skytear sets necessary for
this match, and the rules in use:

    Rules: Living Rules 2020-10-06
    Map: Ashen Pass
    Sets:
      - Starter Box
      - Kurumo
      - Liothan
      - Nupten
      - Into Ashes

The `North` and `South` fields describe the two sides of the battle,
which can be single players, or teams of several players:

    North: Clinton
    South: rlcordan aka Kirito(Ryan)

or:

    North:
      - Mickey
      - Minnie
    South:
      - Donald
      - Daisy

The `Decks` field shows which heroes and matching decks are brought by
both sides.  A `Deck` is primarily made of a set of `Heroes` (each
coming with their deck of cards, listed with their count) and a choice
of an `Outsider`.  The selection of heroes, outsiders and cards should
match the `Sets` declaration.

Additional informations can be given for a deck, like a `Name`, and a
`URL` to find its description.

**FIXME** the following example shows a deck on which the transcriber
has incomplete information (no deck information at all for some
heroes, nature of cards but not their count for others).  It has also
no clear information about URLs, just guesses which are shown using
the non-standard `maybe-url` field:

    Decks:
      N:
        Name: Flip Ya For Real
        Heroes:
          Setheru:
            Vampirism:
            SkytearDischarge:
            ClearMind:
            Doom:
          Yami:
            PsychicShield:
            Doom:
            TimeGlitch:
          Hogosai:
            Impervious:
            QuickShot:
            ShieldSlam:
          Corjof:
          Kichie:
            SkytearDischarge:
            ClearMind:
            Unstoppable:
          Akhuti:
        Outsider: DarkVigilante
      S:
        Name: Suicide Squad
        # precise deck version is unsure
        maybe-url: https://community.playskytear.com/decks/ESaXsG6x76Hps2eJJXsP ?
          https://community.playskytear.com/decks/GjlAAfulSdkZJEMFwydb ?
        Heroes:
          Shyllavi:
          Freyhel:
          Brylvar:
          Astryda:
          Gullbjarn:
          Ekhrit:
        Outsider: Outsider

The game objectives drawn intially are listed as:

    Objectives:
      - Onslaught
      - TheTamer

## elements, attributes, actions

### players and board

- each hex gets named by coordinates as shown [in these annotated
  maps](hex-coordinates.md), with a1 being the bottom left corner when
  the map is in its "cannonical orientation"

- an area is a set of hexes, and is described as such by a
  comma-separated list of hexes belonging to the area.  All hexes in
  the area can be listed, but it is sufficient to list enough of them
  that make the area placement unambiguous:

      {on Outsider activation}
        .to(b4,b5,c5)

  or:

      {sinkhole placement centered on c4}
        .choose:d5,b3

- some special locations not on the board get their own name:
  - `reserve` a place for heroes that will enter play later
    (eg. fourth hero in Ashen Pass)
  - `VoidPalace` when Kumaya's ultimate brings heroes

### _elements_: heroes, outsiders, nexus, towers, minions, pillars, illusions

- elements belonging to a player are written using the player's
  abbreviated side and the element's name, separated by a dot:

      N.Kichie

- this extends to outsiders when activated by a player during minion
  phase:

      S.Outsider

- elements which there can be several instances of are made unique by
  specifying their location hex, separated by a `@`:

      N.minion@d5

  or:

      S.pillar@c3

  or:

      S.tower@a1

### attributes: HP, conditions

- attributes of an element belong to that element, and are similarly
  written:

      N.Kichie.HP
      S.Freyhel.conditions

- setting the value of an element's attribute (eg. on game setup) is
  written with an _equal_ sign:

      N.Kichie.HP=14

- changing the value of an element's attribute is done by specifying
  the addition or removal with a `+` or `-` sign, and optionally the
  resulting value with an _equal_ sign:

      S.Astryda.HP-2=7

  or:

      S.Shyllavi.HP+1=17

  or:

      S.Freyhel.conditions+shapeshifted

- a common special case is when all conditions get removed from a hero
  (eg. on exhaustion), and where the final "no condition" state is
  written as:

      S.Shyllavi.conditions-marked=

- valid attributes for player:

  - `minion@HEX`
  - `pillar@HEX` if player has any Taulot heroes

- valid attributes for any element:

  - `HP`: integer with a max value

- valid attributes for heroes and outsiders:

  - `AP` remaining action points

  - `conditions`: set of flags from the list:

    - `slow`
    - `fast`
    - `frenzy`
    - `disarmed`
    - `marked`
    - `shapeshifted`

  - `illusion` for Nupten heroes only

### actions

- actions of a given player or element are also similarly written (as
  in most computer languages):

      S.flip

  or:

      N.Kichie.move

- (WiP) valid actions for player:

  - level 0
    - `draw(N):CARDS`
    - `flip(N):CARDS`
    - `flip:CARD` short for `flip(1)`
    - `search_draw:CARD` see `Neclalen.action_ConsultingTheHeart`
    - `predict(N):CARDS`
    - `discard(CARD)` eg. following `predict` or `draw`
    - `to_deck(CARD)` following `predict`
    - `shuffle`
    - `spawn(HEX)` spawn minion
    - `pillar(HEX)` if player has any Taulot heroes
    - `objectives` player's progress towards game objectives:

          N.objectives.Onslaught+1=2

  - level 1
    - `choose:XXX` FIXME loosely defined, probably bad (lead, AoE,
      CagedSoul, ...)
    - `replace(EFFECT.PROPERTY,NEWVALUE)` FIXME loosely defined (see
      `Redirect`, `SoulSwap`)
- (WiP) valid actions for any element:

  - level 0
    - `to(HEX)`
    - `remove` just remove for the game.  For heroes and outsiders,
      means they won't come back in this game.

- (WiP) valid actions for activatable elements (heroes and outsiders):

  - level 0
    - `swap(ELEMENT)` exchange position with ELEMENT
  - level 1
    - `activate`
    - `end_activation` effect pushed on the stack at the same time a
      hero gets exhausted
    - `move`
    - `attack`
    - `skirmish`

- (WiP) valid actions for hero:

  - level 0
    - `ready`
    - `exhaust`
    - `defeated` triggers conditions removal, discard power cards in
      hero zone, removal of background effects from passive skills
    - `lead(CARD)`
    - `reveal_lead:CARD`
    - `manaburn:CARD` put card from deck on hero
    - `discard(CARD)`
    - `schedule_respawn(turn=N)` normal death with physical move of the
      hero and its HP token
  - level 1
    - `worship`
    - `damage(ENEMY)` inflict damage to specified ENEMY as a result of
      an effect.  Implicit on effects which by nature target an enemy
      for damage (eg. `attack`).  Especially useful when the damage gets
      inflicted by someone else than the caster:

          + S.Shyllavi.Revenge(target=DarkVigilante)
          - S.Freyhel.damage(DarkVigilante)

    - `.copy(EFFECT,TARGET,TARGET)` making a copy of an EFFECT, changing
      the caster to this hero, and possibly changing effects (see
      `reaction_Refract`)

- (WiP) valid actions for control token:

  - level 0
    - `to(HEX)`

- parameters of such an action are grouped after the action name and
  enclosed in parenthesis.  Parameters are named, and separated from
  the given value by an _equal_ sign.  If more than one parameter is
  needed, they are separated by a _comma_:

      S.Astryda.attack(target=N.minion@d5)

  or:

      S.Astryda.reaction_Redirect(target_hero=N.Yami,target_effect=reaction_KneelBeforeMe)

- playing a card is another such actions:

      N.Kichie.reaction_SkytearDischarge

  or:

      S.Freyhel.reaction_Sacrifice(target=S.Astryda)

  or:

      N.Kichie.action_CurseOfYears(target=S.Gullbjarn)

- using an active skill is another:

      N.Hogosai.PurifyingFire

- some actions produce an outcome, which is then written with a
  _colon_ as separator:

      S.flip:Sinkhole(+0)

### omitting element

- a special action is hero/outsider activation: it sets the activated
  hero/outsider as _default element_, which does not have to be
  repeated each time an attribute or action of that element is
  mentionned:

      S.Shyllavi.activate
      .worship
      .move
      .lead

- symetrically there is a action for hero exhaustion, which should
  usually be at the end of an activation (except eg. when the
  activated hero died, or got exhausted as an effect during the turn),
  and is often tied to a condition clearing (which need not be
  specified when there is no condition to remove):

      .exhaust
      .conditions-marked=

- player specification on a player action can be omitted similarly,
  for the owner of the active hero:

      N.Hogosai.activate
      ...
      .lead
      .choose:hand

### other

- some global games actions change the state of the game without being
  associated with any given player:
  - level 0
    - start new turn by moving token onto specified number

          turn(2)

## the stack

Syntactic elements in this section (`+`, `-`, `<`) belong to
information level 3.

- generation of an effect is introduced by a "+" sign, and usually made
  of a single statement:

      + S.Astryda.attack(target=N.minion@d5)

  but can also contain multiple statements (whitespace can be added to
  make the grouping of the statements more clear):

      + S.Astryda.activate
      ...
      + .worship
        .exhaust
        .conditions-marked=

- an effect resolution is introduced by a "-" sign, and may be
  composed of several statements

      + .attack(target=N.minion@e6)
      - S.flip:Nourish(+1)
        N.minion@e6.remove

- a hero playing a reaction card to reply to an effect being
  generated, and the resolution of those effects can be written as:

      + N.Sakoshi.attack(S.Shyllavi)
      + S.Shyllavi.reaction_Dodge(target=e3)
      - S.Shyllavi.to(e3)
      - {effect cancelled}

- whitespace can be added to make the stack more clear, by indenting
  everything hapenning between the generation of an event and the end
  of the matching resolution:

      + N.Sakoshi.attack(S.Shyllavi)
        + S.Shyllavi.reaction_Dodge(target=e3)
        - S.Shyllavi.to(e3)
      - {effect cancelled}

- after the current effect gets resolved one of the following happens:

  - a new effect is generated (reaction to the effect on top of the
    stack, or new action from active hero), introduced by a `+` character
  - the next effect on the stack starts to resolve, introduced by a
    `-` character
  - a statement not touching the stack happens

  This last case has to be handled specially: although we usually use
  indentation for clarity, the whitespace is not significant, and
  without any special markup we could not tell whether the new
  statement belongs to the previous resolution or not.  In this case
  we use the '<' character to mark the end of resolution and the
  removal of the effect from the stack:

      + .lead
      - .choose:deck {revealed FeralVitality(3) at 12:41}
        <
      
      .exhaust
      .conditions-marked=

  or:

      + N.Yami.ShadowOfTheReaper
      - N.Yami.to(a3)
        N.Yami.flip:TimeGlitch{+2}
        S.Astryda.HP-2=0
        <
      S.Astryda.defeated

## lasting effects

This section covers both background effects (triggers and
replacements), and simple effects that remain for some time (eg. until
end of activation, or while a hero is not defeated).

Syntactic elements in this section belong to information level 4, and
as such are not part of this notation proper.  They can appear as
comments, though, and are partly formalized.

- lasting effects are global game attributes.  Many of them are
  already adequately tracked by the game material (passive skills,
  simple background effects generated by power cards on a hero):

      + DarkVigilante.Slash
      - {effects+.Slash}
        .skirmish
        .damage(S.minion@d5) {use_effect(.Slash)}

  Other cards like _Mind Palace_ or Neclalen's _The Heart Claims_ can
  remove the cards without removing their effects.  Some other effects
  (eg. _Lord of the Many_) are computed from a given game state and do
  not change until they are removed.

- triggered effects themselves can be mentionned following the
  (sub-)effect triggering them, then later get their own stack frame:

      + S.Astryda.activate
      -

      + .skirmish
        + N.Kichie.reaction_SkytearDischarge
        - {triggers+N.Kichie.SkytearDischarge}
      - .to(b3)
        {trigger(N.Kichie.SkytearDischarge)}
      + N.Kichie.SkytearDischarge
      - N.flip:PsychicShield(+3)
        .HP-5=9

- some effects are triggered on effect generation, when it is already
  valid to put them on the stack:

      + Shafathi.skirmish
        + .SchemersProtection {trigger}
        - .predict(1)
      -
